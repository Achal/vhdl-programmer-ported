﻿Public Class ArchitectureForm
    Public arch As Architecture
    Private Sub bt_insert_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bt_insert.Click
        arch = New Architecture()
        arch.name = tb_architecture_name.Text
        arch.entity_name = cb_entity_name.Text
        reset()
        Me.DialogResult = DialogResult.OK
    End Sub

    Private Sub bt_cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bt_cancel.Click
        Me.DialogResult = DialogResult.Cancel
    End Sub

    Private Sub bt_reset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bt_reset.Click
        reset()
    End Sub
    Private Sub reset()
        ''tb_architecture_name.Text = ""
        tb_architecture_name.ResetText()
        cb_entity_name.ResetText()
    End Sub
End Class