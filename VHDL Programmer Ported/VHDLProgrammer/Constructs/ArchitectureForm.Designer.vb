﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ArchitectureForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.bt_cancel = New System.Windows.Forms.Button()
        Me.bt_reset = New System.Windows.Forms.Button()
        Me.bt_insert = New System.Windows.Forms.Button()
        Me.cb_entity_name = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.tb_architecture_name = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.bt_cancel)
        Me.Panel1.Controls.Add(Me.bt_reset)
        Me.Panel1.Controls.Add(Me.bt_insert)
        Me.Panel1.Controls.Add(Me.cb_entity_name)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.tb_architecture_name)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(268, 89)
        Me.Panel1.TabIndex = 0
        '
        'bt_cancel
        '
        Me.bt_cancel.Location = New System.Drawing.Point(190, 62)
        Me.bt_cancel.Name = "bt_cancel"
        Me.bt_cancel.Size = New System.Drawing.Size(75, 23)
        Me.bt_cancel.TabIndex = 6
        Me.bt_cancel.Text = "Cancel"
        Me.bt_cancel.UseVisualStyleBackColor = True
        '
        'bt_reset
        '
        Me.bt_reset.Location = New System.Drawing.Point(96, 62)
        Me.bt_reset.Name = "bt_reset"
        Me.bt_reset.Size = New System.Drawing.Size(75, 23)
        Me.bt_reset.TabIndex = 5
        Me.bt_reset.Text = "Reset"
        Me.bt_reset.UseVisualStyleBackColor = True
        '
        'bt_insert
        '
        Me.bt_insert.Location = New System.Drawing.Point(0, 63)
        Me.bt_insert.Name = "bt_insert"
        Me.bt_insert.Size = New System.Drawing.Size(75, 23)
        Me.bt_insert.TabIndex = 4
        Me.bt_insert.Text = "Insert"
        Me.bt_insert.UseVisualStyleBackColor = True
        '
        'cb_entity_name
        '
        Me.cb_entity_name.FormattingEnabled = True
        Me.cb_entity_name.Location = New System.Drawing.Point(68, 31)
        Me.cb_entity_name.Name = "cb_entity_name"
        Me.cb_entity_name.Size = New System.Drawing.Size(197, 21)
        Me.cb_entity_name.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(3, 34)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(64, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Entity Name"
        '
        'tb_architecture_name
        '
        Me.tb_architecture_name.Location = New System.Drawing.Point(68, 4)
        Me.tb_architecture_name.Name = "tb_architecture_name"
        Me.tb_architecture_name.Size = New System.Drawing.Size(197, 20)
        Me.tb_architecture_name.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 7)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(35, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Name"
        '
        'ArchitectureForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(292, 113)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "ArchitectureForm"
        Me.Text = "ArchitectureForm"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents bt_cancel As System.Windows.Forms.Button
    Friend WithEvents bt_reset As System.Windows.Forms.Button
    Friend WithEvents bt_insert As System.Windows.Forms.Button
    Friend WithEvents cb_entity_name As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents tb_architecture_name As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
