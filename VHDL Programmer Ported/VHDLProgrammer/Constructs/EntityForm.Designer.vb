﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EntityForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.p_entity_outter = New System.Windows.Forms.Panel()
        Me.b_enitity_cancel = New System.Windows.Forms.Button()
        Me.b_entity_reset = New System.Windows.Forms.Button()
        Me.b_entity_add = New System.Windows.Forms.Button()
        Me.tc_pg = New System.Windows.Forms.TabControl()
        Me.tp_port = New System.Windows.Forms.TabPage()
        Me.lb_port_list = New System.Windows.Forms.ListBox()
        Me.bt_port_remove = New System.Windows.Forms.Button()
        Me.bt_port_add = New System.Windows.Forms.Button()
        Me.pn_port_range = New System.Windows.Forms.Panel()
        Me.tb_port_range_end2 = New System.Windows.Forms.TextBox()
        Me.tb_port_range_end1 = New System.Windows.Forms.TextBox()
        Me.cb_port_indexing_scheme = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cb_port_type = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cb_port_mode = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.tb_port_name = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.tp_generic = New System.Windows.Forms.TabPage()
        Me.lb_generic_list = New System.Windows.Forms.ListBox()
        Me.bt_generic_remove = New System.Windows.Forms.Button()
        Me.bt_generic_add = New System.Windows.Forms.Button()
        Me.pn_geeneric_range = New System.Windows.Forms.Panel()
        Me.tb_generic_range_end2 = New System.Windows.Forms.TextBox()
        Me.tb_generic_range_end1 = New System.Windows.Forms.TextBox()
        Me.cb_generic_indexing_scheme = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cb_generic_type = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.tb_generic_name = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.tb_entity_name = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.p_entity_outter.SuspendLayout()
        Me.tc_pg.SuspendLayout()
        Me.tp_port.SuspendLayout()
        Me.pn_port_range.SuspendLayout()
        Me.tp_generic.SuspendLayout()
        Me.pn_geeneric_range.SuspendLayout()
        Me.SuspendLayout()
        '
        'p_entity_outter
        '
        Me.p_entity_outter.Controls.Add(Me.b_enitity_cancel)
        Me.p_entity_outter.Controls.Add(Me.b_entity_reset)
        Me.p_entity_outter.Controls.Add(Me.b_entity_add)
        Me.p_entity_outter.Controls.Add(Me.tc_pg)
        Me.p_entity_outter.Controls.Add(Me.tb_entity_name)
        Me.p_entity_outter.Controls.Add(Me.Label1)
        Me.p_entity_outter.Location = New System.Drawing.Point(12, 12)
        Me.p_entity_outter.Name = "p_entity_outter"
        Me.p_entity_outter.Size = New System.Drawing.Size(301, 420)
        Me.p_entity_outter.TabIndex = 0
        '
        'b_enitity_cancel
        '
        Me.b_enitity_cancel.Location = New System.Drawing.Point(211, 394)
        Me.b_enitity_cancel.Name = "b_enitity_cancel"
        Me.b_enitity_cancel.Size = New System.Drawing.Size(75, 23)
        Me.b_enitity_cancel.TabIndex = 5
        Me.b_enitity_cancel.Text = "Cancel"
        Me.b_enitity_cancel.UseVisualStyleBackColor = True
        '
        'b_entity_reset
        '
        Me.b_entity_reset.Location = New System.Drawing.Point(114, 394)
        Me.b_entity_reset.Name = "b_entity_reset"
        Me.b_entity_reset.Size = New System.Drawing.Size(75, 23)
        Me.b_entity_reset.TabIndex = 4
        Me.b_entity_reset.Text = "Reset"
        Me.b_entity_reset.UseVisualStyleBackColor = True
        '
        'b_entity_add
        '
        Me.b_entity_add.Location = New System.Drawing.Point(15, 394)
        Me.b_entity_add.Name = "b_entity_add"
        Me.b_entity_add.Size = New System.Drawing.Size(75, 23)
        Me.b_entity_add.TabIndex = 3
        Me.b_entity_add.Text = "Insert"
        Me.b_entity_add.UseVisualStyleBackColor = True
        '
        'tc_pg
        '
        Me.tc_pg.Controls.Add(Me.tp_port)
        Me.tc_pg.Controls.Add(Me.tp_generic)
        Me.tc_pg.Location = New System.Drawing.Point(15, 28)
        Me.tc_pg.Name = "tc_pg"
        Me.tc_pg.SelectedIndex = 0
        Me.tc_pg.Size = New System.Drawing.Size(271, 360)
        Me.tc_pg.TabIndex = 2
        '
        'tp_port
        '
        Me.tp_port.Controls.Add(Me.lb_port_list)
        Me.tp_port.Controls.Add(Me.bt_port_remove)
        Me.tp_port.Controls.Add(Me.bt_port_add)
        Me.tp_port.Controls.Add(Me.pn_port_range)
        Me.tp_port.Controls.Add(Me.cb_port_type)
        Me.tp_port.Controls.Add(Me.Label4)
        Me.tp_port.Controls.Add(Me.cb_port_mode)
        Me.tp_port.Controls.Add(Me.Label3)
        Me.tp_port.Controls.Add(Me.tb_port_name)
        Me.tp_port.Controls.Add(Me.Label2)
        Me.tp_port.Location = New System.Drawing.Point(4, 22)
        Me.tp_port.Name = "tp_port"
        Me.tp_port.Padding = New System.Windows.Forms.Padding(3)
        Me.tp_port.Size = New System.Drawing.Size(263, 334)
        Me.tp_port.TabIndex = 0
        Me.tp_port.Text = "Ports"
        Me.tp_port.UseVisualStyleBackColor = True
        '
        'lb_port_list
        '
        Me.lb_port_list.FormattingEnabled = True
        Me.lb_port_list.HorizontalScrollbar = True
        Me.lb_port_list.Location = New System.Drawing.Point(12, 162)
        Me.lb_port_list.Name = "lb_port_list"
        Me.lb_port_list.Size = New System.Drawing.Size(242, 160)
        Me.lb_port_list.TabIndex = 9
        '
        'bt_port_remove
        '
        Me.bt_port_remove.Location = New System.Drawing.Point(133, 131)
        Me.bt_port_remove.Name = "bt_port_remove"
        Me.bt_port_remove.Size = New System.Drawing.Size(75, 23)
        Me.bt_port_remove.TabIndex = 8
        Me.bt_port_remove.Text = "Remove"
        Me.bt_port_remove.UseVisualStyleBackColor = True
        '
        'bt_port_add
        '
        Me.bt_port_add.Location = New System.Drawing.Point(51, 132)
        Me.bt_port_add.Name = "bt_port_add"
        Me.bt_port_add.Size = New System.Drawing.Size(75, 23)
        Me.bt_port_add.TabIndex = 7
        Me.bt_port_add.Text = "Add"
        Me.bt_port_add.UseVisualStyleBackColor = True
        '
        'pn_port_range
        '
        Me.pn_port_range.Controls.Add(Me.tb_port_range_end2)
        Me.pn_port_range.Controls.Add(Me.tb_port_range_end1)
        Me.pn_port_range.Controls.Add(Me.cb_port_indexing_scheme)
        Me.pn_port_range.Controls.Add(Me.Label5)
        Me.pn_port_range.Location = New System.Drawing.Point(6, 86)
        Me.pn_port_range.Name = "pn_port_range"
        Me.pn_port_range.Size = New System.Drawing.Size(248, 40)
        Me.pn_port_range.TabIndex = 6
        '
        'tb_port_range_end2
        '
        Me.tb_port_range_end2.Location = New System.Drawing.Point(182, 16)
        Me.tb_port_range_end2.Name = "tb_port_range_end2"
        Me.tb_port_range_end2.Size = New System.Drawing.Size(63, 20)
        Me.tb_port_range_end2.TabIndex = 3
        '
        'tb_port_range_end1
        '
        Me.tb_port_range_end1.Location = New System.Drawing.Point(6, 17)
        Me.tb_port_range_end1.Name = "tb_port_range_end1"
        Me.tb_port_range_end1.Size = New System.Drawing.Size(66, 20)
        Me.tb_port_range_end1.TabIndex = 2
        '
        'cb_port_indexing_scheme
        '
        Me.cb_port_indexing_scheme.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cb_port_indexing_scheme.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cb_port_indexing_scheme.FormattingEnabled = True
        Me.cb_port_indexing_scheme.Items.AddRange(New Object() {"Not an Array", "DOWNTO", "TO"})
        Me.cb_port_indexing_scheme.Location = New System.Drawing.Point(78, 17)
        Me.cb_port_indexing_scheme.Name = "cb_port_indexing_scheme"
        Me.cb_port_indexing_scheme.Size = New System.Drawing.Size(97, 21)
        Me.cb_port_indexing_scheme.TabIndex = 1
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(3, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(39, 13)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Range"
        '
        'cb_port_type
        '
        Me.cb_port_type.FormattingEnabled = True
        Me.cb_port_type.Location = New System.Drawing.Point(75, 59)
        Me.cb_port_type.Name = "cb_port_type"
        Me.cb_port_type.Size = New System.Drawing.Size(179, 21)
        Me.cb_port_type.TabIndex = 5
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(9, 62)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(31, 13)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Type"
        '
        'cb_port_mode
        '
        Me.cb_port_mode.FormattingEnabled = True
        Me.cb_port_mode.Items.AddRange(New Object() {"IN", "OUT", "INOUT", "BUFFER", "LINKAGE"})
        Me.cb_port_mode.Location = New System.Drawing.Point(75, 31)
        Me.cb_port_mode.Name = "cb_port_mode"
        Me.cb_port_mode.Size = New System.Drawing.Size(179, 21)
        Me.cb_port_mode.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(9, 34)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(34, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Mode"
        '
        'tb_port_name
        '
        Me.tb_port_name.Location = New System.Drawing.Point(75, 4)
        Me.tb_port_name.Name = "tb_port_name"
        Me.tb_port_name.Size = New System.Drawing.Size(179, 20)
        Me.tb_port_name.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(9, 7)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(35, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Name"
        '
        'tp_generic
        '
        Me.tp_generic.Controls.Add(Me.lb_generic_list)
        Me.tp_generic.Controls.Add(Me.bt_generic_remove)
        Me.tp_generic.Controls.Add(Me.bt_generic_add)
        Me.tp_generic.Controls.Add(Me.pn_geeneric_range)
        Me.tp_generic.Controls.Add(Me.cb_generic_type)
        Me.tp_generic.Controls.Add(Me.Label7)
        Me.tp_generic.Controls.Add(Me.tb_generic_name)
        Me.tp_generic.Controls.Add(Me.Label9)
        Me.tp_generic.Location = New System.Drawing.Point(4, 22)
        Me.tp_generic.Name = "tp_generic"
        Me.tp_generic.Padding = New System.Windows.Forms.Padding(3)
        Me.tp_generic.Size = New System.Drawing.Size(263, 334)
        Me.tp_generic.TabIndex = 1
        Me.tp_generic.Text = "Generics"
        Me.tp_generic.UseVisualStyleBackColor = True
        '
        'lb_generic_list
        '
        Me.lb_generic_list.FormattingEnabled = True
        Me.lb_generic_list.Location = New System.Drawing.Point(13, 136)
        Me.lb_generic_list.MultiColumn = True
        Me.lb_generic_list.Name = "lb_generic_list"
        Me.lb_generic_list.Size = New System.Drawing.Size(242, 186)
        Me.lb_generic_list.TabIndex = 19
        '
        'bt_generic_remove
        '
        Me.bt_generic_remove.Location = New System.Drawing.Point(134, 102)
        Me.bt_generic_remove.Name = "bt_generic_remove"
        Me.bt_generic_remove.Size = New System.Drawing.Size(75, 23)
        Me.bt_generic_remove.TabIndex = 18
        Me.bt_generic_remove.Text = "Remove"
        Me.bt_generic_remove.UseVisualStyleBackColor = True
        '
        'bt_generic_add
        '
        Me.bt_generic_add.Location = New System.Drawing.Point(52, 103)
        Me.bt_generic_add.Name = "bt_generic_add"
        Me.bt_generic_add.Size = New System.Drawing.Size(75, 23)
        Me.bt_generic_add.TabIndex = 17
        Me.bt_generic_add.Text = "Add"
        Me.bt_generic_add.UseVisualStyleBackColor = True
        '
        'pn_geeneric_range
        '
        Me.pn_geeneric_range.Controls.Add(Me.tb_generic_range_end2)
        Me.pn_geeneric_range.Controls.Add(Me.tb_generic_range_end1)
        Me.pn_geeneric_range.Controls.Add(Me.cb_generic_indexing_scheme)
        Me.pn_geeneric_range.Controls.Add(Me.Label6)
        Me.pn_geeneric_range.Location = New System.Drawing.Point(7, 57)
        Me.pn_geeneric_range.Name = "pn_geeneric_range"
        Me.pn_geeneric_range.Size = New System.Drawing.Size(248, 40)
        Me.pn_geeneric_range.TabIndex = 16
        '
        'tb_generic_range_end2
        '
        Me.tb_generic_range_end2.Location = New System.Drawing.Point(182, 16)
        Me.tb_generic_range_end2.Name = "tb_generic_range_end2"
        Me.tb_generic_range_end2.Size = New System.Drawing.Size(63, 20)
        Me.tb_generic_range_end2.TabIndex = 3
        '
        'tb_generic_range_end1
        '
        Me.tb_generic_range_end1.Location = New System.Drawing.Point(6, 17)
        Me.tb_generic_range_end1.Name = "tb_generic_range_end1"
        Me.tb_generic_range_end1.Size = New System.Drawing.Size(66, 20)
        Me.tb_generic_range_end1.TabIndex = 2
        '
        'cb_generic_indexing_scheme
        '
        Me.cb_generic_indexing_scheme.FormattingEnabled = True
        Me.cb_generic_indexing_scheme.Location = New System.Drawing.Point(78, 17)
        Me.cb_generic_indexing_scheme.Name = "cb_generic_indexing_scheme"
        Me.cb_generic_indexing_scheme.Size = New System.Drawing.Size(97, 21)
        Me.cb_generic_indexing_scheme.TabIndex = 1
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(3, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(39, 13)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Range"
        '
        'cb_generic_type
        '
        Me.cb_generic_type.FormattingEnabled = True
        Me.cb_generic_type.Location = New System.Drawing.Point(76, 30)
        Me.cb_generic_type.Name = "cb_generic_type"
        Me.cb_generic_type.Size = New System.Drawing.Size(179, 21)
        Me.cb_generic_type.TabIndex = 15
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(10, 33)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(31, 13)
        Me.Label7.TabIndex = 14
        Me.Label7.Text = "Type"
        '
        'tb_generic_name
        '
        Me.tb_generic_name.Location = New System.Drawing.Point(76, 4)
        Me.tb_generic_name.Name = "tb_generic_name"
        Me.tb_generic_name.Size = New System.Drawing.Size(179, 20)
        Me.tb_generic_name.TabIndex = 11
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(10, 7)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(35, 13)
        Me.Label9.TabIndex = 10
        Me.Label9.Text = "Name"
        '
        'tb_entity_name
        '
        Me.tb_entity_name.Location = New System.Drawing.Point(94, 2)
        Me.tb_entity_name.Name = "tb_entity_name"
        Me.tb_entity_name.Size = New System.Drawing.Size(179, 20)
        Me.tb_entity_name.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(28, 5)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(35, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Name"
        '
        'EntityForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(325, 444)
        Me.Controls.Add(Me.p_entity_outter)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "EntityForm"
        Me.Text = "Entity"
        Me.p_entity_outter.ResumeLayout(False)
        Me.p_entity_outter.PerformLayout()
        Me.tc_pg.ResumeLayout(False)
        Me.tp_port.ResumeLayout(False)
        Me.tp_port.PerformLayout()
        Me.pn_port_range.ResumeLayout(False)
        Me.pn_port_range.PerformLayout()
        Me.tp_generic.ResumeLayout(False)
        Me.tp_generic.PerformLayout()
        Me.pn_geeneric_range.ResumeLayout(False)
        Me.pn_geeneric_range.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents p_entity_outter As System.Windows.Forms.Panel
    Friend WithEvents tc_pg As System.Windows.Forms.TabControl
    Friend WithEvents tp_port As System.Windows.Forms.TabPage
    Friend WithEvents pn_port_range As System.Windows.Forms.Panel
    Friend WithEvents tb_port_range_end2 As System.Windows.Forms.TextBox
    Friend WithEvents tb_port_range_end1 As System.Windows.Forms.TextBox
    Friend WithEvents cb_port_indexing_scheme As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cb_port_type As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cb_port_mode As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents tb_port_name As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents tp_generic As System.Windows.Forms.TabPage
    Friend WithEvents tb_entity_name As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents bt_port_remove As System.Windows.Forms.Button
    Friend WithEvents bt_port_add As System.Windows.Forms.Button
    Friend WithEvents b_enitity_cancel As System.Windows.Forms.Button
    Friend WithEvents b_entity_reset As System.Windows.Forms.Button
    Friend WithEvents b_entity_add As System.Windows.Forms.Button
    Friend WithEvents lb_port_list As System.Windows.Forms.ListBox
    Friend WithEvents lb_generic_list As System.Windows.Forms.ListBox
    Friend WithEvents bt_generic_remove As System.Windows.Forms.Button
    Friend WithEvents bt_generic_add As System.Windows.Forms.Button
    Friend WithEvents pn_geeneric_range As System.Windows.Forms.Panel
    Friend WithEvents tb_generic_range_end2 As System.Windows.Forms.TextBox
    Friend WithEvents tb_generic_range_end1 As System.Windows.Forms.TextBox
    Friend WithEvents cb_generic_indexing_scheme As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cb_generic_type As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents tb_generic_name As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class
