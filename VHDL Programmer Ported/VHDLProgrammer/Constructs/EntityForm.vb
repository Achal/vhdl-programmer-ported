﻿Public Class EntityForm

    Public entity As Entity

    Private Sub bt_port_add_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bt_port_add.Click
        Dim port_string As String = encodePort(tb_port_name.Text, cb_port_mode.Text.ToUpper, cb_port_type.Text.ToUpper)
        If cb_port_indexing_scheme.Text = "DOWNTO" Or cb_port_indexing_scheme.Text = "TO" Then
            port_string += encodePortRange(cb_port_indexing_scheme.Text, tb_port_range_end1.Text, tb_port_range_end2.Text)
        End If
        lb_port_list.Items.Add(port_string)
    End Sub

    Private Sub bt_port_remove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bt_port_remove.Click
        lb_port_list.Items.Remove(lb_port_list.SelectedItem)
    End Sub

    Private Function encodePort(ByVal name As String, ByVal mode As String, ByVal type As String) As String
        Dim str As String = ""
        str += "Port " + name + " of mode " + mode + " and type " + type
        Return str
    End Function

    Private Function encodePortRange(ByVal indexing_scheme As String, ByVal starting_index As String, ByVal ending_index As String) As String
        Dim str As String = ""
        str += " Ranging " + starting_index + " " + indexing_scheme + " " + ending_index
        Return str
    End Function

    Private Function decodePort(ByVal port_str As String) As Port
        Dim port As Port = New Port()
        Dim str() As String = port_str.Split(" ")
        port.name = str(1)
        port.mode = str(4)
        port.data_type = New EnumeratedType()
        port.data_type.name = str(7)
        If (str.Length > 8) Then
            Dim array_type As ArrayType = New ArrayType()
            array_type.name = str(7)
            array_type.indexing_scheme = str(10)
            array_type.starting_index = str(9)
            array_type.ending_index = str(11)
            port.data_type = array_type
        End If
        Return (port)
    End Function



    Private Sub bt_generic_add_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bt_generic_add.Click
        Dim generic_string As String = encodeGeneric(tb_generic_name.Text, cb_generic_type.Text.ToUpper)
        If cb_generic_indexing_scheme.Text = "DOWNTO" Or cb_generic_indexing_scheme.Text = "TO" Then
            generic_string += encodePortRange(cb_generic_indexing_scheme.Text, tb_generic_range_end1.Text, tb_generic_range_end2.Text)
        End If
        lb_generic_list.Items.Add(generic_string)
    End Sub

    Private Sub bt_generic_remove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bt_generic_remove.Click
        lb_generic_list.Items.Remove(lb_generic_list.SelectedItem)
    End Sub

    Private Function encodeGeneric(ByVal name As String, ByVal type As String) As String
        Dim str As String = ""
        str += "Generic " + name + " of type " + type
        Return str
    End Function

    Private Function encodeGenericRange(ByVal indexing_scheme As String, ByVal starting_index As String, ByVal ending_index As String) As String
        Dim str As String = ""
        str += " Ranging " + starting_index + " " + indexing_scheme + " " + ending_index
        Return str
    End Function

    Private Function decodeGeneric(ByVal generic_str As String) As Generic
        Dim generic As Generic = New Generic()
        Dim str() As String = generic_str.Split(" ")
        generic.name = str(1)
        generic.data_type = New EnumeratedType()
        generic.data_type.name = str(4)
        If (str.Length > 5) Then
            Dim array_type As ArrayType = New ArrayType()
            array_type.name = str(4)
            array_type.indexing_scheme = str(7)
            array_type.starting_index = str(6)
            array_type.ending_index = str(8)
            generic.data_type = array_type
        End If
        Return (generic)
    End Function



    Private Sub b_entity_add_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles b_entity_add.Click
        entity = New Entity()
        entity.name = tb_entity_name.Text
        For Each port_str As String In lb_port_list.Items
            entity.addPort(decodePort(port_str))
        Next
        For Each generic_str As String In lb_generic_list.Items
            entity.addGeneric(decodeGeneric(generic_str))
        Next
        reset()
        Me.DialogResult() = DialogResult.OK
    End Sub

    Private Sub b_enitity_cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles b_enitity_cancel.Click
        Me.DialogResult = DialogResult.Cancel
    End Sub

    Private Sub reset()
        tb_entity_name.ResetText()
        tb_generic_name.ResetText()
        tb_generic_range_end1.ResetText()
        tb_generic_range_end2.ResetText()
        tb_port_name.ResetText()
        tb_port_range_end1.ResetText()
        tb_port_range_end2.ResetText()
        cb_generic_indexing_scheme.ResetText()
        cb_generic_type.ResetText()
        cb_port_indexing_scheme.ResetText()
        cb_port_mode.ResetText()
        cb_port_type.ResetText()
        lb_generic_list.Items.Clear()
        lb_port_list.Items.Clear()
    End Sub

    Private Sub b_entity_reset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles b_entity_reset.Click
        reset()
    End Sub
End Class