﻿Public Class MainForm

    Private Sub EntityToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EntityToolStripMenuItem.Click
        If EntityForm.ShowDialog() = DialogResult.OK Then
            rtb_code.Text += EntityForm.entity.toString()
        End If
    End Sub

    Private Sub ArchitectureToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ArchitectureToolStripMenuItem.Click
        If ArchitectureForm.ShowDialog() = DialogResult.OK Then
            rtb_code.Text += ArchitectureForm.arch.toString()
        End If
    End Sub

    Private Sub IntegerToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles IntegerToolStripMenuItem.Click
        If IntegerForm.ShowDialog() = DialogResult.OK Then
            rtb_code.Text += IntegerForm.integer_type.decString()
        End If
    End Sub

    Private Sub FloatToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FloatToolStripMenuItem.Click
        If FloatForm.ShowDialog() = DialogResult.OK Then
            rtb_code.Text += FloatForm.float_type.decString()
        End If
    End Sub

    Private Sub EnumeratedToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EnumeratedToolStripMenuItem.Click
        If EnumeratedForm.ShowDialog() = DialogResult.OK Then
            rtb_code.Text += EnumeratedForm.enumerated_type.decString()
        End If
    End Sub

    Private Sub ArrayToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ArrayToolStripMenuItem.Click
        If ArrayForm.ShowDialog() = DialogResult.OK Then
            rtb_code.Text += ArrayForm.array_type.decString()
        End If
    End Sub

    Private Sub RecordToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RecordToolStripMenuItem.Click
        If RecordForm.ShowDialog() = DialogResult.OK Then
            rtb_code.Text += RecordForm.record_type.decString()
        End If
    End Sub
End Class