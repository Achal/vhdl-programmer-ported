﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EnumeratedForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.bt_cancel = New System.Windows.Forms.Button()
        Me.tb_type_name = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.bt_reset = New System.Windows.Forms.Button()
        Me.bt_insert = New System.Windows.Forms.Button()
        Me.lt_identidier = New System.Windows.Forms.ListBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.tb_identifier_name = New System.Windows.Forms.TextBox()
        Me.bt_identifier_remove = New System.Windows.Forms.Button()
        Me.bt_identifier_add = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'bt_cancel
        '
        Me.bt_cancel.Location = New System.Drawing.Point(194, 227)
        Me.bt_cancel.Name = "bt_cancel"
        Me.bt_cancel.Size = New System.Drawing.Size(75, 23)
        Me.bt_cancel.TabIndex = 22
        Me.bt_cancel.Text = "Cancel"
        Me.bt_cancel.UseVisualStyleBackColor = True
        '
        'tb_type_name
        '
        Me.tb_type_name.Location = New System.Drawing.Point(95, 9)
        Me.tb_type_name.Name = "tb_type_name"
        Me.tb_type_name.Size = New System.Drawing.Size(165, 20)
        Me.tb_type_name.TabIndex = 18
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(35, 13)
        Me.Label1.TabIndex = 17
        Me.Label1.Text = "Name"
        '
        'bt_reset
        '
        Me.bt_reset.Location = New System.Drawing.Point(103, 227)
        Me.bt_reset.Name = "bt_reset"
        Me.bt_reset.Size = New System.Drawing.Size(75, 23)
        Me.bt_reset.TabIndex = 21
        Me.bt_reset.Text = "Reset"
        Me.bt_reset.UseVisualStyleBackColor = True
        '
        'bt_insert
        '
        Me.bt_insert.Location = New System.Drawing.Point(12, 227)
        Me.bt_insert.Name = "bt_insert"
        Me.bt_insert.Size = New System.Drawing.Size(75, 23)
        Me.bt_insert.TabIndex = 20
        Me.bt_insert.Text = "Insert"
        Me.bt_insert.UseVisualStyleBackColor = True
        '
        'lt_identidier
        '
        Me.lt_identidier.FormattingEnabled = True
        Me.lt_identidier.Location = New System.Drawing.Point(15, 87)
        Me.lt_identidier.Name = "lt_identidier"
        Me.lt_identidier.Size = New System.Drawing.Size(254, 134)
        Me.lt_identidier.TabIndex = 23
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 39)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(47, 13)
        Me.Label2.TabIndex = 24
        Me.Label2.Text = "Identifier"
        '
        'tb_identifier_name
        '
        Me.tb_identifier_name.Location = New System.Drawing.Point(95, 36)
        Me.tb_identifier_name.Name = "tb_identifier_name"
        Me.tb_identifier_name.Size = New System.Drawing.Size(165, 20)
        Me.tb_identifier_name.TabIndex = 25
        '
        'bt_identifier_remove
        '
        Me.bt_identifier_remove.Location = New System.Drawing.Point(136, 62)
        Me.bt_identifier_remove.Name = "bt_identifier_remove"
        Me.bt_identifier_remove.Size = New System.Drawing.Size(75, 23)
        Me.bt_identifier_remove.TabIndex = 26
        Me.bt_identifier_remove.Text = "Remove"
        Me.bt_identifier_remove.UseVisualStyleBackColor = True
        '
        'bt_identifier_add
        '
        Me.bt_identifier_add.Location = New System.Drawing.Point(55, 62)
        Me.bt_identifier_add.Name = "bt_identifier_add"
        Me.bt_identifier_add.Size = New System.Drawing.Size(75, 23)
        Me.bt_identifier_add.TabIndex = 27
        Me.bt_identifier_add.Text = "Add"
        Me.bt_identifier_add.UseVisualStyleBackColor = True
        '
        'Enumerated
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(281, 262)
        Me.Controls.Add(Me.bt_identifier_add)
        Me.Controls.Add(Me.bt_identifier_remove)
        Me.Controls.Add(Me.tb_identifier_name)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lt_identidier)
        Me.Controls.Add(Me.bt_cancel)
        Me.Controls.Add(Me.tb_type_name)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.bt_reset)
        Me.Controls.Add(Me.bt_insert)
        Me.Name = "Enumerated"
        Me.Text = "Enumerated"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents bt_cancel As System.Windows.Forms.Button
    Friend WithEvents tb_type_name As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents bt_reset As System.Windows.Forms.Button
    Friend WithEvents bt_insert As System.Windows.Forms.Button
    Friend WithEvents lt_identidier As System.Windows.Forms.ListBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents tb_identifier_name As System.Windows.Forms.TextBox
    Friend WithEvents bt_identifier_remove As System.Windows.Forms.Button
    Friend WithEvents bt_identifier_add As System.Windows.Forms.Button
End Class
