﻿Public Class EnumeratedForm
    Public enumerated_type As EnumeratedType

    Private Sub bt_identifier_add_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bt_identifier_add.Click
        lt_identidier.Items.Add(tb_identifier_name.Text)
    End Sub

    Private Sub bt_identifier_remove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bt_identifier_remove.Click
        lt_identidier.Items.Remove(lt_identidier.SelectedItem)
    End Sub

    Private Sub bt_insert_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bt_insert.Click
        enumerated_type = New EnumeratedType(tb_type_name.Text)
        For Each identifier As String In lt_identidier.Items
            enumerated_type.addIdentifier(identifier)
        Next
        Reset()
        Me.DialogResult = DialogResult.OK
    End Sub

    Private Sub bt_reset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bt_reset.Click
        Reset()
    End Sub
    Public Sub reset()
        tb_type_name.Text = ""
        tb_identifier_name.Text = ""
        lt_identidier.Items.Clear()
    End Sub

    Private Sub bt_cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bt_cancel.Click
        Me.DialogResult = DialogResult.Cancel
    End Sub
End Class