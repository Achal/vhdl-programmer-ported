﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RecordForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.bt_element_add = New System.Windows.Forms.Button()
        Me.bt_element_remove = New System.Windows.Forms.Button()
        Me.tb_element_name = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lt_element = New System.Windows.Forms.ListBox()
        Me.bt_cancel = New System.Windows.Forms.Button()
        Me.tb_type_name = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.bt_reset = New System.Windows.Forms.Button()
        Me.bt_insert = New System.Windows.Forms.Button()
        Me.pn_port_range = New System.Windows.Forms.Panel()
        Me.tb_element_range_end2 = New System.Windows.Forms.TextBox()
        Me.tb_element_range_end1 = New System.Windows.Forms.TextBox()
        Me.cb_element_indexing_scheme = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cb_element_type = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.pn_port_range.SuspendLayout()
        Me.SuspendLayout()
        '
        'bt_element_add
        '
        Me.bt_element_add.Location = New System.Drawing.Point(52, 141)
        Me.bt_element_add.Name = "bt_element_add"
        Me.bt_element_add.Size = New System.Drawing.Size(75, 23)
        Me.bt_element_add.TabIndex = 37
        Me.bt_element_add.Text = "Add"
        Me.bt_element_add.UseVisualStyleBackColor = True
        '
        'bt_element_remove
        '
        Me.bt_element_remove.Location = New System.Drawing.Point(133, 141)
        Me.bt_element_remove.Name = "bt_element_remove"
        Me.bt_element_remove.Size = New System.Drawing.Size(75, 23)
        Me.bt_element_remove.TabIndex = 36
        Me.bt_element_remove.Text = "Remove"
        Me.bt_element_remove.UseVisualStyleBackColor = True
        '
        'tb_element_name
        '
        Me.tb_element_name.Location = New System.Drawing.Point(97, 38)
        Me.tb_element_name.Name = "tb_element_name"
        Me.tb_element_name.Size = New System.Drawing.Size(165, 20)
        Me.tb_element_name.TabIndex = 35
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(14, 41)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(74, 13)
        Me.Label2.TabIndex = 34
        Me.Label2.Text = "Element name"
        '
        'lt_element
        '
        Me.lt_element.FormattingEnabled = True
        Me.lt_element.Location = New System.Drawing.Point(12, 166)
        Me.lt_element.Name = "lt_element"
        Me.lt_element.Size = New System.Drawing.Size(254, 134)
        Me.lt_element.TabIndex = 33
        '
        'bt_cancel
        '
        Me.bt_cancel.Location = New System.Drawing.Point(191, 306)
        Me.bt_cancel.Name = "bt_cancel"
        Me.bt_cancel.Size = New System.Drawing.Size(75, 23)
        Me.bt_cancel.TabIndex = 32
        Me.bt_cancel.Text = "Cancel"
        Me.bt_cancel.UseVisualStyleBackColor = True
        '
        'tb_type_name
        '
        Me.tb_type_name.Location = New System.Drawing.Point(97, 11)
        Me.tb_type_name.Name = "tb_type_name"
        Me.tb_type_name.Size = New System.Drawing.Size(165, 20)
        Me.tb_type_name.TabIndex = 29
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(14, 11)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(35, 13)
        Me.Label1.TabIndex = 28
        Me.Label1.Text = "Name"
        '
        'bt_reset
        '
        Me.bt_reset.Location = New System.Drawing.Point(100, 306)
        Me.bt_reset.Name = "bt_reset"
        Me.bt_reset.Size = New System.Drawing.Size(75, 23)
        Me.bt_reset.TabIndex = 31
        Me.bt_reset.Text = "Reset"
        Me.bt_reset.UseVisualStyleBackColor = True
        '
        'bt_insert
        '
        Me.bt_insert.Location = New System.Drawing.Point(9, 306)
        Me.bt_insert.Name = "bt_insert"
        Me.bt_insert.Size = New System.Drawing.Size(75, 23)
        Me.bt_insert.TabIndex = 30
        Me.bt_insert.Text = "Insert"
        Me.bt_insert.UseVisualStyleBackColor = True
        '
        'pn_port_range
        '
        Me.pn_port_range.Controls.Add(Me.tb_element_range_end2)
        Me.pn_port_range.Controls.Add(Me.tb_element_range_end1)
        Me.pn_port_range.Controls.Add(Me.cb_element_indexing_scheme)
        Me.pn_port_range.Controls.Add(Me.Label5)
        Me.pn_port_range.Location = New System.Drawing.Point(15, 95)
        Me.pn_port_range.Name = "pn_port_range"
        Me.pn_port_range.Size = New System.Drawing.Size(248, 40)
        Me.pn_port_range.TabIndex = 40
        '
        'tb_element_range_end2
        '
        Me.tb_element_range_end2.Location = New System.Drawing.Point(182, 16)
        Me.tb_element_range_end2.Name = "tb_element_range_end2"
        Me.tb_element_range_end2.Size = New System.Drawing.Size(63, 20)
        Me.tb_element_range_end2.TabIndex = 3
        '
        'tb_element_range_end1
        '
        Me.tb_element_range_end1.Location = New System.Drawing.Point(6, 17)
        Me.tb_element_range_end1.Name = "tb_element_range_end1"
        Me.tb_element_range_end1.Size = New System.Drawing.Size(66, 20)
        Me.tb_element_range_end1.TabIndex = 2
        '
        'cb_element_indexing_scheme
        '
        Me.cb_element_indexing_scheme.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cb_element_indexing_scheme.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cb_element_indexing_scheme.FormattingEnabled = True
        Me.cb_element_indexing_scheme.Items.AddRange(New Object() {"Not an Array", "DOWNTO", "TO"})
        Me.cb_element_indexing_scheme.Location = New System.Drawing.Point(78, 17)
        Me.cb_element_indexing_scheme.Name = "cb_element_indexing_scheme"
        Me.cb_element_indexing_scheme.Size = New System.Drawing.Size(97, 21)
        Me.cb_element_indexing_scheme.TabIndex = 1
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(3, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(39, 13)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Range"
        '
        'cb_element_type
        '
        Me.cb_element_type.FormattingEnabled = True
        Me.cb_element_type.Location = New System.Drawing.Point(97, 68)
        Me.cb_element_type.Name = "cb_element_type"
        Me.cb_element_type.Size = New System.Drawing.Size(166, 21)
        Me.cb_element_type.TabIndex = 39
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(14, 71)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(31, 13)
        Me.Label4.TabIndex = 38
        Me.Label4.Text = "Type"
        '
        'Record
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 339)
        Me.Controls.Add(Me.pn_port_range)
        Me.Controls.Add(Me.cb_element_type)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.bt_element_add)
        Me.Controls.Add(Me.bt_element_remove)
        Me.Controls.Add(Me.tb_element_name)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lt_element)
        Me.Controls.Add(Me.bt_cancel)
        Me.Controls.Add(Me.tb_type_name)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.bt_reset)
        Me.Controls.Add(Me.bt_insert)
        Me.Name = "Record"
        Me.Text = "Record"
        Me.pn_port_range.ResumeLayout(False)
        Me.pn_port_range.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents bt_element_add As System.Windows.Forms.Button
    Friend WithEvents bt_element_remove As System.Windows.Forms.Button
    Friend WithEvents tb_element_name As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lt_element As System.Windows.Forms.ListBox
    Friend WithEvents bt_cancel As System.Windows.Forms.Button
    Friend WithEvents tb_type_name As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents bt_reset As System.Windows.Forms.Button
    Friend WithEvents bt_insert As System.Windows.Forms.Button
    Friend WithEvents pn_port_range As System.Windows.Forms.Panel
    Friend WithEvents tb_element_range_end2 As System.Windows.Forms.TextBox
    Friend WithEvents tb_element_range_end1 As System.Windows.Forms.TextBox
    Friend WithEvents cb_element_indexing_scheme As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cb_element_type As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
End Class
