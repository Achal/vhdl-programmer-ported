﻿Public Class RecordForm
    Public record_type As RecordType

    Private Sub bt_element_add_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bt_element_add.Click
        Dim element_string As String = encodeElement(tb_element_name.Text, cb_element_type.Text.ToUpper)
        If cb_element_indexing_scheme.Text = "DOWNTO" Or cb_element_indexing_scheme.Text = "TO" Then
            element_string += encodeElementRange(cb_element_indexing_scheme.Text, tb_element_range_end1.Text, tb_element_range_end2.Text)
        End If
        lt_element.Items.Add(element_string)
    End Sub

    Private Sub bt_element_remove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bt_element_remove.Click
        lt_element.Items.Remove(lt_element.SelectedItem)
    End Sub
    Private Function encodeElement(ByVal name As String, ByVal type As String) As String
        Dim str As String = ""
        str += "Element " + name + " of type " + type
        Return str
    End Function

    Private Function encodeElementRange(ByVal indexing_scheme As String, ByVal starting_index As String, ByVal ending_index As String) As String
        Dim str As String = ""
        str += " Ranging " + starting_index + " " + indexing_scheme + " " + ending_index
        Return str
    End Function

    Private Function decodeElement(ByVal port_str As String) As SubType
        Dim sub_type As SubType = New SubType()
        Dim str() As String = port_str.Split(" ")
        sub_type.name = str(1)
        sub_type.data_type = New EnumeratedType()
        sub_type.data_type.name = str(4)
        If (str.Length > 5) Then
            Dim array_type As ArrayType = New ArrayType()
            array_type.name = str(4)
            array_type.indexing_scheme = str(7)
            array_type.starting_index = str(6)
            array_type.ending_index = str(8)
            sub_type.data_type = array_type
        End If
        Return (sub_type)
    End Function

    Private Sub bt_insert_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bt_insert.Click
        record_type = New RecordType
        record_type.name = tb_type_name.Text
        For Each element_str As String In lt_element.Items
            record_type.addElement(decodeElement(element_str))
        Next
        Reset()
        Me.DialogResult() = DialogResult.OK
    End Sub
    Private Sub reset()
        tb_type_name.ResetText()
        tb_element_name.ResetText()
        cb_element_type.ResetText()
        cb_element_indexing_scheme.ResetText()
        tb_element_range_end1.ResetText()
        tb_element_range_end2.ResetText()
        lt_element.Items.Clear()
    End Sub

    Private Sub bt_reset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bt_reset.Click
        reset()
    End Sub

    Private Sub bt_cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bt_cancel.Click
        Me.DialogResult = DialogResult.Cancel
    End Sub
End Class