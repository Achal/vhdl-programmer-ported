﻿Public Class ArrayForm
    Public array_type As ArrayType
    Private Sub bt_insert_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bt_insert.Click
        array_type = New ArrayType(tb_type_name.Text, New EnumeratedType(), cb_type_indexing_scheme.Text, tb_type_range_end1.Text, tb_type_range_end2.Text)
        Reset()
        Me.DialogResult = DialogResult.OK
    End Sub

    Private Sub bt_reset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bt_reset.Click
        Reset()
    End Sub

    Private Sub bt_cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bt_cancel.Click
        Me.DialogResult = DialogResult.Cancel
    End Sub
    Public Sub reset()
        tb_type_name.Text = ""
        cb_type_name.Text = ""
        cb_type_indexing_scheme.Text = ""
        tb_type_range_end1.Text = ""
        tb_type_range_end2.Text = ""
    End Sub
End Class