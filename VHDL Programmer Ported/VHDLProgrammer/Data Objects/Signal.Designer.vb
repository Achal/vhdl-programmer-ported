﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Signal
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pn_port_range = New System.Windows.Forms.Panel()
        Me.tb_range_end2 = New System.Windows.Forms.TextBox()
        Me.tb_range_end1 = New System.Windows.Forms.TextBox()
        Me.cb_indexing_scheme = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cb_signal_type = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cb_signal_mode = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.tb_signal_name = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.bt_cancel = New System.Windows.Forms.Button()
        Me.bt_reset = New System.Windows.Forms.Button()
        Me.bt_insert = New System.Windows.Forms.Button()
        Me.pn_port_range.SuspendLayout()
        Me.SuspendLayout()
        '
        'pn_port_range
        '
        Me.pn_port_range.Controls.Add(Me.tb_range_end2)
        Me.pn_port_range.Controls.Add(Me.tb_range_end1)
        Me.pn_port_range.Controls.Add(Me.cb_indexing_scheme)
        Me.pn_port_range.Controls.Add(Me.Label5)
        Me.pn_port_range.Location = New System.Drawing.Point(9, 88)
        Me.pn_port_range.Name = "pn_port_range"
        Me.pn_port_range.Size = New System.Drawing.Size(248, 40)
        Me.pn_port_range.TabIndex = 13
        '
        'tb_range_end2
        '
        Me.tb_range_end2.Location = New System.Drawing.Point(182, 16)
        Me.tb_range_end2.Name = "tb_range_end2"
        Me.tb_range_end2.Size = New System.Drawing.Size(63, 20)
        Me.tb_range_end2.TabIndex = 3
        '
        'tb_range_end1
        '
        Me.tb_range_end1.Location = New System.Drawing.Point(6, 17)
        Me.tb_range_end1.Name = "tb_range_end1"
        Me.tb_range_end1.Size = New System.Drawing.Size(66, 20)
        Me.tb_range_end1.TabIndex = 2
        '
        'cb_indexing_scheme
        '
        Me.cb_indexing_scheme.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cb_indexing_scheme.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cb_indexing_scheme.FormattingEnabled = True
        Me.cb_indexing_scheme.Items.AddRange(New Object() {"Not an Array", "DOWNTO", "TO"})
        Me.cb_indexing_scheme.Location = New System.Drawing.Point(78, 17)
        Me.cb_indexing_scheme.Name = "cb_indexing_scheme"
        Me.cb_indexing_scheme.Size = New System.Drawing.Size(97, 21)
        Me.cb_indexing_scheme.TabIndex = 1
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(3, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(39, 13)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Range"
        '
        'cb_signal_type
        '
        Me.cb_signal_type.FormattingEnabled = True
        Me.cb_signal_type.Location = New System.Drawing.Point(78, 61)
        Me.cb_signal_type.Name = "cb_signal_type"
        Me.cb_signal_type.Size = New System.Drawing.Size(179, 21)
        Me.cb_signal_type.TabIndex = 12
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(12, 64)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(31, 13)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Type"
        '
        'cb_signal_mode
        '
        Me.cb_signal_mode.FormattingEnabled = True
        Me.cb_signal_mode.Items.AddRange(New Object() {"IN", "OUT", "INOUT", "BUFFER", "LINKAGE"})
        Me.cb_signal_mode.Location = New System.Drawing.Point(78, 33)
        Me.cb_signal_mode.Name = "cb_signal_mode"
        Me.cb_signal_mode.Size = New System.Drawing.Size(179, 21)
        Me.cb_signal_mode.TabIndex = 10
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 36)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(34, 13)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "Mode"
        '
        'tb_signal_name
        '
        Me.tb_signal_name.Location = New System.Drawing.Point(78, 6)
        Me.tb_signal_name.Name = "tb_signal_name"
        Me.tb_signal_name.Size = New System.Drawing.Size(179, 20)
        Me.tb_signal_name.TabIndex = 8
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(35, 13)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Name"
        '
        'bt_cancel
        '
        Me.bt_cancel.Location = New System.Drawing.Point(182, 134)
        Me.bt_cancel.Name = "bt_cancel"
        Me.bt_cancel.Size = New System.Drawing.Size(75, 23)
        Me.bt_cancel.TabIndex = 16
        Me.bt_cancel.Text = "Cancel"
        Me.bt_cancel.UseVisualStyleBackColor = True
        '
        'bt_reset
        '
        Me.bt_reset.Location = New System.Drawing.Point(97, 134)
        Me.bt_reset.Name = "bt_reset"
        Me.bt_reset.Size = New System.Drawing.Size(75, 23)
        Me.bt_reset.TabIndex = 15
        Me.bt_reset.Text = "Reset"
        Me.bt_reset.UseVisualStyleBackColor = True
        '
        'bt_insert
        '
        Me.bt_insert.Location = New System.Drawing.Point(9, 134)
        Me.bt_insert.Name = "bt_insert"
        Me.bt_insert.Size = New System.Drawing.Size(75, 23)
        Me.bt_insert.TabIndex = 14
        Me.bt_insert.Text = "Insert"
        Me.bt_insert.UseVisualStyleBackColor = True
        '
        'Signal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(271, 167)
        Me.Controls.Add(Me.bt_cancel)
        Me.Controls.Add(Me.bt_reset)
        Me.Controls.Add(Me.bt_insert)
        Me.Controls.Add(Me.pn_port_range)
        Me.Controls.Add(Me.cb_signal_type)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.cb_signal_mode)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.tb_signal_name)
        Me.Controls.Add(Me.Label2)
        Me.name = "Signal"
        Me.Text = "Signal"
        Me.pn_port_range.ResumeLayout(False)
        Me.pn_port_range.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents pn_port_range As System.Windows.Forms.Panel
    Friend WithEvents tb_range_end2 As System.Windows.Forms.TextBox
    Friend WithEvents tb_range_end1 As System.Windows.Forms.TextBox
    Friend WithEvents cb_indexing_scheme As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cb_signal_type As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cb_signal_mode As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents tb_signal_name As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents bt_cancel As System.Windows.Forms.Button
    Friend WithEvents bt_reset As System.Windows.Forms.Button
    Friend WithEvents bt_insert As System.Windows.Forms.Button
End Class
