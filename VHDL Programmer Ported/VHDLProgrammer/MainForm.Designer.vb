﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InsertToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConstructsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EntityToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ArchitectureToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TypeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ScalarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IntegerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FloatToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EnumeratedToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CompositeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ArrayToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RecordToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SubTypeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.rtb_code = New System.Windows.Forms.RichTextBox()
        Me.DataObjectsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SignalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VaraiableToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PortToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GenericToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConstantToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.EditToolStripMenuItem, Me.InsertToolStripMenuItem, Me.HelpToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(523, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'EditToolStripMenuItem
        '
        Me.EditToolStripMenuItem.Name = "EditToolStripMenuItem"
        Me.EditToolStripMenuItem.Size = New System.Drawing.Size(39, 20)
        Me.EditToolStripMenuItem.Text = "Edit"
        '
        'InsertToolStripMenuItem
        '
        Me.InsertToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ConstructsToolStripMenuItem, Me.DataObjectsToolStripMenuItem, Me.TypeToolStripMenuItem})
        Me.InsertToolStripMenuItem.Name = "InsertToolStripMenuItem"
        Me.InsertToolStripMenuItem.Size = New System.Drawing.Size(48, 20)
        Me.InsertToolStripMenuItem.Text = "Insert"
        '
        'ConstructsToolStripMenuItem
        '
        Me.ConstructsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EntityToolStripMenuItem, Me.ArchitectureToolStripMenuItem})
        Me.ConstructsToolStripMenuItem.Name = "ConstructsToolStripMenuItem"
        Me.ConstructsToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.ConstructsToolStripMenuItem.Text = "Constructs"
        '
        'EntityToolStripMenuItem
        '
        Me.EntityToolStripMenuItem.Name = "EntityToolStripMenuItem"
        Me.EntityToolStripMenuItem.Size = New System.Drawing.Size(139, 22)
        Me.EntityToolStripMenuItem.Text = "Entity"
        '
        'ArchitectureToolStripMenuItem
        '
        Me.ArchitectureToolStripMenuItem.Name = "ArchitectureToolStripMenuItem"
        Me.ArchitectureToolStripMenuItem.Size = New System.Drawing.Size(139, 22)
        Me.ArchitectureToolStripMenuItem.Text = "Architecture"
        '
        'TypeToolStripMenuItem
        '
        Me.TypeToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ScalarToolStripMenuItem, Me.CompositeToolStripMenuItem, Me.SubTypeToolStripMenuItem})
        Me.TypeToolStripMenuItem.Name = "TypeToolStripMenuItem"
        Me.TypeToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.TypeToolStripMenuItem.Text = "Type"
        '
        'ScalarToolStripMenuItem
        '
        Me.ScalarToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.IntegerToolStripMenuItem, Me.FloatToolStripMenuItem, Me.EnumeratedToolStripMenuItem})
        Me.ScalarToolStripMenuItem.Name = "ScalarToolStripMenuItem"
        Me.ScalarToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.ScalarToolStripMenuItem.Text = "Scalar"
        '
        'IntegerToolStripMenuItem
        '
        Me.IntegerToolStripMenuItem.Name = "IntegerToolStripMenuItem"
        Me.IntegerToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.IntegerToolStripMenuItem.Text = "Integer"
        '
        'FloatToolStripMenuItem
        '
        Me.FloatToolStripMenuItem.Name = "FloatToolStripMenuItem"
        Me.FloatToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.FloatToolStripMenuItem.Text = "Float"
        '
        'EnumeratedToolStripMenuItem
        '
        Me.EnumeratedToolStripMenuItem.Name = "EnumeratedToolStripMenuItem"
        Me.EnumeratedToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.EnumeratedToolStripMenuItem.Text = "Enumerated"
        '
        'CompositeToolStripMenuItem
        '
        Me.CompositeToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ArrayToolStripMenuItem, Me.RecordToolStripMenuItem})
        Me.CompositeToolStripMenuItem.Name = "CompositeToolStripMenuItem"
        Me.CompositeToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.CompositeToolStripMenuItem.Text = "Composite"
        '
        'ArrayToolStripMenuItem
        '
        Me.ArrayToolStripMenuItem.Name = "ArrayToolStripMenuItem"
        Me.ArrayToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.ArrayToolStripMenuItem.Text = "Array"
        '
        'RecordToolStripMenuItem
        '
        Me.RecordToolStripMenuItem.Name = "RecordToolStripMenuItem"
        Me.RecordToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.RecordToolStripMenuItem.Text = "Record"
        '
        'SubTypeToolStripMenuItem
        '
        Me.SubTypeToolStripMenuItem.Name = "SubTypeToolStripMenuItem"
        Me.SubTypeToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.SubTypeToolStripMenuItem.Text = "Sub Type"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'rtb_code
        '
        Me.rtb_code.Dock = System.Windows.Forms.DockStyle.Fill
        Me.rtb_code.Location = New System.Drawing.Point(0, 24)
        Me.rtb_code.Name = "rtb_code"
        Me.rtb_code.Size = New System.Drawing.Size(523, 425)
        Me.rtb_code.TabIndex = 1
        Me.rtb_code.Text = ""
        '
        'DataObjectsToolStripMenuItem
        '
        Me.DataObjectsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SignalToolStripMenuItem, Me.VaraiableToolStripMenuItem, Me.PortToolStripMenuItem, Me.GenericToolStripMenuItem, Me.ConstantToolStripMenuItem})
        Me.DataObjectsToolStripMenuItem.Name = "DataObjectsToolStripMenuItem"
        Me.DataObjectsToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.DataObjectsToolStripMenuItem.Text = "Data Objects"
        '
        'SignalToolStripMenuItem
        '
        Me.SignalToolStripMenuItem.Name = "SignalToolStripMenuItem"
        Me.SignalToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.SignalToolStripMenuItem.Text = "Signal"
        '
        'VaraiableToolStripMenuItem
        '
        Me.VaraiableToolStripMenuItem.Name = "VaraiableToolStripMenuItem"
        Me.VaraiableToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.VaraiableToolStripMenuItem.Text = "Varaiable"
        '
        'PortToolStripMenuItem
        '
        Me.PortToolStripMenuItem.Name = "PortToolStripMenuItem"
        Me.PortToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.PortToolStripMenuItem.Text = "Port"
        '
        'GenericToolStripMenuItem
        '
        Me.GenericToolStripMenuItem.Name = "GenericToolStripMenuItem"
        Me.GenericToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.GenericToolStripMenuItem.Text = "Generic"
        '
        'ConstantToolStripMenuItem
        '
        Me.ConstantToolStripMenuItem.Name = "ConstantToolStripMenuItem"
        Me.ConstantToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.ConstantToolStripMenuItem.Text = "Constant"
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(523, 449)
        Me.Controls.Add(Me.rtb_code)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "MainForm"
        Me.Text = "MainForm"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EditToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InsertToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HelpToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents rtb_code As System.Windows.Forms.RichTextBox
    Friend WithEvents ConstructsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EntityToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ArchitectureToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TypeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ScalarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IntegerToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FloatToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EnumeratedToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CompositeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ArrayToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RecordToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SubTypeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DataObjectsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SignalToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VaraiableToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PortToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GenericToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConstantToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
