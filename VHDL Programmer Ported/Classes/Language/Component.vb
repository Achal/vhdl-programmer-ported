﻿Public Class Component
    Public entity As Entity
    Public instance_number As Integer
    Public Sub New()

    End Sub
    Public Sub New(ByVal entity As Entity)
        Me.entity = entity
    End Sub
    Public Function decString() As String
        Dim str As String = entity.toString()
        str = str.Replace("ENTITY", "COMPONENT")
        Return str
    End Function
    Public Function usgString() As String
        Dim str As String
        str = entity.name + instance_number.ToString + " : " + entity.name + " "
        str += "GENERIC MAP ("
        For Each generic In entity.getAllGenerics
            str += generic.name + ", "
        Next
        str = str.Substring(0, str.LastIndexOf(",")) + ");" + vbNewLine
        Return str
        str += "PORT MAP ("
        For Each port In entity.getAllPorts
            str += port.name + ", "
        Next
        str = str.Substring(0, str.LastIndexOf(",")) + ");"
        Return str
    End Function
End Class
