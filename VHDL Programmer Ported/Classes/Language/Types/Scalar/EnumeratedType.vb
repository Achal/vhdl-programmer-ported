﻿Public Class EnumeratedType
    Inherits DataType

    Private identifier_list As List(Of String)
    Public Sub New()
        MyBase.new()
        identifier_list = New List(Of String)
    End Sub
    Public Sub New(ByVal name As String)
        MyBase.new(name)
        identifier_list = New List(Of String)
    End Sub

    Public Sub addIdentifier(ByVal identifier As String)
        identifier_list.Add(identifier)
    End Sub
    Public Sub addIdentifiers(ByVal identifiers() As String)
        identifier_list.AddRange(identifiers)
    End Sub

    Public Sub addIdentifier(ByVal index As Integer, ByVal identifier As String)
        identifier_list.Insert(index, identifier)
    End Sub

    Public Function getIdentifier(ByVal index As Integer) As String
        Return identifier_list.Item(index)
    End Function

    Public Function getIdentifiers() As String()
        Return identifier_list.ToArray()
    End Function

    Public Overrides Function decString() As String
        Dim str As String
        str = "TYPE " + name + " is ( "
        For Each identifier In getIdentifiers()
            str += identifier + ", "
        Next
        str = str.Substring(0, str.LastIndexOf(",")) + " );"
        Return str
    End Function
    Public Overrides Function usgString() As String
        Return name
    End Function
End Class
