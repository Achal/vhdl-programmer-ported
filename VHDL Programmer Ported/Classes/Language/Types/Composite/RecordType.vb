﻿Public Class RecordType
    Inherits DataType
    Public elements As List(Of SubType)
    Public Sub New()
        Me.name = ""
        elements = New List(Of SubType)
    End Sub
    Public Sub New(ByVal name As String)
        Me.name = name
        elements = New List(Of SubType)
    End Sub
    Public Sub addElement(ByVal identifier As SubType)
        elements.Add(identifier)
    End Sub

    Public Function getElement(ByVal index As Integer) As SubType
        Return elements.Item(index)
    End Function

    Public Function getElements() As SubType()
        Return elements.ToArray()
    End Function
    Public Overrides Function decString() As String
        Dim str As String
        str = "TYPE " + name + " is" + vbNewLine
        str += vbTab + "RECORD" + vbNewLine
        For Each sub_type As SubType In getElements()
            str += vbTab + vbTab + recordString(sub_type) + vbNewLine
        Next
        str += vbTab + "END RECORD;"
        Return str
    End Function

    Public Overrides Function usgString() As String
        Return name
    End Function

    Private Function recordString(ByVal sub_type As SubType) As String
        Dim str As String
        str = sub_type.name + " : " + sub_type.data_type.usgString + _
            " RANGE " + sub_type.starting_index + " " + sub_type.indexing_scheme + _
            " " + sub_type.ending_index + ";"
        Return str
    End Function

End Class
