﻿Public Class SubType
    Inherits DataType
    Public data_type As DataType
    Public indexing_scheme As String
    Public starting_index, ending_index As String
    Public Sub New()
        MyBase.new("")
        Me.indexing_scheme = ""
        Me.starting_index = ""
        Me.ending_index = ""
    End Sub
    Public Sub New(ByVal name As String, ByVal type As String)
        MyBase.new(name)
        Me.indexing_scheme = ""
        Me.starting_index = ""
        Me.ending_index = ""
    End Sub

    Public Sub New(ByVal name As String, ByVal type As String, _
                        ByVal indexing_scheme As String, _
                         Optional ByVal starting_index As String = "0", Optional ByVal ending_index As String = "0")
        MyBase.new(name)
        Me.indexing_scheme = indexing_scheme
        Me.starting_index = starting_index
        Me.ending_index = ending_index
    End Sub

    Public Overrides Function decString() As String
        Dim str As String
        str = "TYPE " + name + " is " + data_type.usgString
        If indexing_scheme <> "" Then
            str += " range " + starting_index + " " _
                + indexing_scheme + " " + ending_index
        End If
        str += ";"
        Return str
    End Function

    Public Overrides Function usgString() As String
        Return name
    End Function
End Class
