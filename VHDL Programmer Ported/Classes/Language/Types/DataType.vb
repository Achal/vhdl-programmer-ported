﻿Public MustInherit Class DataType

    Public name As String
    Public Sub New()
        Me.name = ""
    End Sub
    Public Sub New(ByVal name As String)
        Me.name = name
    End Sub
    Public MustOverride Function decString() As String
    Public MustOverride Function usgString() As String
End Class
