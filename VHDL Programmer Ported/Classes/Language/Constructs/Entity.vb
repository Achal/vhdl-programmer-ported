﻿Public Class Entity

    Public name As String
    Private ports As List(Of Port)
    Private generics As List(Of Generic)

    Public Sub New()
        ports = New List(Of Port)
        generics = New List(Of Generic)
    End Sub
    Public Sub New(ByVal name As String)
        Me.name = name
        ports = New List(Of Port)
        generics = New List(Of Generic)
    End Sub

    Public Sub addPort(ByVal port As Port)
        ports.Add(port)
    End Sub

    Public Sub addPorts(ByVal port() As Port)
        ports.AddRange(port)
    End Sub

    Public Function removePort(ByVal port_name As String) As Boolean
        Dim sgl As Port
        For i = 0 To ports.Count - 1
            sgl = ports.Item(i)
            If sgl.name = port_name Then
                ports.RemoveAt(i)
                Return True
            End If
        Next

        Return False
    End Function
    Public Sub removeAllPorts()
        ports.Clear()
    End Sub

    Public Function getPort(ByVal index As Integer) As Port
        Return ports.Item(index)
    End Function

    Public Function getAllPorts() As Port()
        Return ports.ToArray
    End Function



    Public Sub addGeneric(ByVal generic As Generic)
        generics.Add(generic)
    End Sub

    Public Sub addGenerics(ByVal generic() As Generic)
        generics.AddRange(generic)
    End Sub

    Public Function removeGernric(ByVal generic_name As String) As Boolean
        Dim gen As Generic
        For i = 0 To generics.Count - 1
            gen = generics.Item(i)
            If gen.name = generic_name Then
                generics.RemoveAt(i)
                Return True
            End If
        Next

        Return False
    End Function
    Public Sub removeAllGenerics()
        generics.Clear()
    End Sub

    Public Function getGernric(ByVal index As Integer) As Generic
        Return generics.Item(index)
    End Function

    Public Function getAllGenerics() As Generic()
        Return generics.ToArray
    End Function

    Public Overrides Function toString() As String
        Dim str As String = ""
        Dim tab As String = "     "
        str += "ENTITY " + name + " IS"
        If (generics.Count > 0) Then
            str += tab + vbNewLine + "GENERIC ("
            Dim gen As Generic
            For i = 0 To generics.Count - 1
                gen = generics.Item(i)
                str += vbNewLine + tab + gen.toString() + ";"
            Next
            str = str.Substring(0, str.Length() - 1) + vbNewLine + tab + ");"
        End If

        If (ports.Count > 0) Then
            str += tab + tab + vbNewLine + "PORT ("
            Dim sgl As Port
            For i = 0 To ports.Count - 1
                sgl = ports.Item(i)
                str += vbNewLine + tab + tab + sgl.toString() + ";"
            Next
            str = str.Substring(0, str.Length() - 1) + vbNewLine + tab + ");"
        End If
        str += vbNewLine + "END ENTITY " + name + " ;"
        Return str
    End Function

End Class
