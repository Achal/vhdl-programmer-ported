﻿Public Class Architecture
    Public name As String
    Public entity_name As String
    Public Sub New()
    End Sub
    Public Sub New(ByVal name As String, ByVal entity_name As String)
        Me.name = name
        Me.entity_name = entity_name
    End Sub
    Public Overrides Function toString() As String
        Dim tab As String = "     "
        Dim str As String
        str = "ARCHITECTURE " + name + " OF " + entity_name + " IS " + vbNewLine
        str += tab + "--declaration statements" + vbNewLine + vbNewLine
        str += "BEGIN" + vbNewLine
        str += tab + "--concurrent statements" + vbNewLine + vbNewLine
        str += "END ARCHITECTURE " + name + ";"
        Return str
    End Function
End Class
