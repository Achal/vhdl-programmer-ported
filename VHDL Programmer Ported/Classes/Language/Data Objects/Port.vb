﻿Public Class Port
    Inherits Signal

    Public mode As String
    Public Sub New()
        MyBase.new()
        Me.mode = ""
    End Sub

    Public Sub New(ByVal name As String, ByVal data_type As DataType, ByVal mode As String)
        MyBase.new(name, data_type)
        Me.mode = mode
    End Sub

    'Public Overrides Function toString() As String
    '    Dim str As String
    '    str = name + " : " + mode + " " + data_type.name
    '    If data_type.GetType = GetType(ArrayType) Then
    '        Dim at As ArrayType = data_type
    '        str = str + " RANGE ( " + at.starting_index + " " + at.indexing_scheme + " " + at.ending_index + " );"
    '    End If
    '    Return str
    'End Function
    Public Overrides Function toString() As String
        Return name + " : " + mode + " " + data_type.usgString + ";"
    End Function
End Class
